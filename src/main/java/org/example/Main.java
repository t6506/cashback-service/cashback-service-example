package org.example;

import org.example.service.CashbackService;

public class Main {
    public static void main(String[] args) {
        CashbackService cashbackService = new CashbackService();
        System.out.println(cashbackService.calculate(3533_35));
        System.out.println(cashbackService.calculate(1_000_000_00));
    }
}
